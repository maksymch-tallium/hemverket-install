#!/bin/bash

# To clone next repos you might be need bitbacket account and be a part of https://packagist.com/orgs/marton-international organisation
# composer config --global --auth http-basic.repo.packagist.com [login] [token]
#
#echo $?

NC="\033[0m";       # Text Reset
YELLOW="\033[0;33m"; # Yellow
RED='\033[0;31m';    # Red
command=""

if [ -d "$PWD/hemverket-laradock" ]; then
  printf "${YELLOW}WARNING: git clone maksymch-tallium/hemverket-laradock will be skipped! $PWD/hemverket-install dir already exists \n${NC}"
else
  command="${command} git clone --single-branch --branch local https://maksymch-tallium@bitbucket.org/maksymch-tallium/hemverket-laradock.git;"
fi

if [ -d "$PWD/highestate-3.0" ]; then
  printf "${YELLOW}WARNING: git clone highestate/highestate-3.0 will be skipped! $PWD/highestate-3.0 dir already exists \n${NC}"
else
  command="${command} git clone --single-branch --branch uat/norway git@bitbucket.org:highestate/highestate-3.0.git;"
fi

if [ -d "$PWD/mypage" ]; then
  printf "${YELLOW}WARNING: git clone highestate/mypage will be skipped! $PWD/mypage dir already exists \n${NC}"
else
  command="${command} git clone --single-branch --branch uat/x git@bitbucket.org:highestate/mypage.git;"
fi

if [ -d "$PWD/object" ]; then
  printf "${YELLOW}WARNING: git clone highestate/object will be skipped! $PWD/object dir already exists \n${NC}"
else
  command="${command} git clone --single-branch --branch uatx git@bitbucket.org:highestate/object.git;"
fi

if [ -d "$PWD/highestateapi" ]; then
  printf "${YELLOW}WARNING: git clone highestate/highestateapi will be skipped! $PWD/highestateapi dir already exists \n${NC}"
else
  command="${command} git clone --single-branch --branch master git@bitbucket.org:highestate/highestateapi.git;"
fi

eval $command;

