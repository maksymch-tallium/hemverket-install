### Mypage installation
1.Run:

```bash
git clone --single-branch --branch uat/x git@bitbucket.org:highestate/mypage.git
```

> To avoid errors during composer dependencies installation you might need composer.lock file 
>
> You can get in from:
`scp marton@uatx-app.highestate.io:/srv/data/mypage/composer.lock composer.lock`
```bash
cd mypage
composer install
```


2.Rename `config/apis.php.example` to `config/apis.php` file and set the required parameters

