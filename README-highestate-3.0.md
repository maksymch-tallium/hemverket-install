### Highestate installation

1.Run:
```bash
git clone --single-branch --branch uat/norway git@bitbucket.org:highestate/highestate-3.0.git
cd highestate-3.0
composer install
```
2.Modify config-file `config/config.inc.php` to setup base settings:
```php
/* Cloudinary API config */
$config['cloudinary_api'] = [
    "cloud_name" => "highestate",
    "api_key"    => "###############",
    "api_secret" => "******************"
];

/* Database settings */
$config['database'] = [
    'host'     => 'mariadb',
//    'port' => 3306,
    'username' => '',
    'password' => '',
    'database' => 'highestate'
];
```

...or just copy from remote on stage:
`scp marton@uatx-app.highestate.io:/srv/data/highestate/config/config.inc.php /config/config.inc.php`
*<sub><sup>notice: check if your user(marton) have sufficient rights to run command </sup></sub>

3.Rename config files:
- `config/loader_config.json.dist` to `config/loader_config.json`
and change config file like so:
```json
{
    "_comment": "Use with caution! Anything set here will override default values and may cause unexpected behaviour. Safe to change are defined below.",
    "production": false,
    "debugLevel": 2,
    "executionTimeRecord": true,
    "redisConfig": {
      "scheme": "tcp",
      "host": "00.000.00.000",
      "port": 0000
    },
    "custom.he_publisher": [
      [
        "localhost"
      ]
    ]
}
```

<sub><sup>This need to run CLI properly</sup></sub> 

- `config/locale.in.php.dist` to `config/locale.in.php`
