### Object installation
1.Run:
```bash
git clone --single-branch --branch uatx git@bitbucket.org:highestate/object.git
cd object
composer install
```
2.Rename `config/config.inc.php.dist` to `config/config.inc.php`

3.Define this in: `config.inc.php`
```
// Idfy
define('IDFY_CLIENT_ID', '');
define('IDFY_CLIENT_SECRET', '');
define('IDFY_CLIENT_DEBUG', false);
```


### BID Process:

1. Make [request](requests/submitBid.http) \
2. Run saving bid process info to db\

tables:
>object_sale_relation \
>object_sale_relation_bid

...

> after sending request when bid was setted. some data sending to queue...

Stack trace:
- [object/html/norway-bidding/src/Api.php](object/html/norway-bidding/src/Api.php)
- [object/html/norway-bidding/src/Route.php](object/html/norway-bidding/src/Route.php) submitBid
- [object/html/norway-bidding/src/Bidding.php](object/html/norway-bidding/src/Bidding.php) #L174
- [object/vendor/highestate/bidding-messaging/src/BidPlace.php](object/vendor/highestate/bidding-messaging/src/BidPlace.php) #L27
- [object/vendor/highestate/bidding-messaging/src/BidPlace.php](object/vendor/highestate/bidding-messaging/src/BidPlace.php) #L58
> ...and transform into signcat link.


### How generate signin url and document?
- [highestate-3.0/config/nginx_rewrite.conf](highestate-3.0/config/nginx_rewrite.conf) \
 Cant understand how this endpoint launched?? `^/api/crypto/v1/(.*)$` on - [object/html/norway-bidding/src/Bidding.php](object/html/norway-bidding/src/Bidding.php) ???
- [highestate-3.0/html/api/crypto/v1/CryptoApi.php](highestate-3.0/html/api/crypto/v1/CryptoApi.php)
- [highestate-3.0/html/api/crypto/v1/modules/Crypto/SignicatApi.php](highestate-3.0/html/api/crypto/v1/modules/Crypto/SignicatApi.php) on #L271
line DocumentGenerator


### Get signin url
1. Make [request](requests/getSignInUrl.http)
2. Stak trace:
    - [object/html/norway-bidding/src/Api.php](object/html/norway-bidding/src/Api.php)
    - [object/html/norway-bidding/src/Route.php](object/html/norway-bidding/src/Route.php)
    - [object/html/norway-bidding/src/Bidding.php](object/html/norway-bidding/src/Bidding.php)
    - [object/html/norway-bidding/src/SigningUrl.php](object/html/norway-bidding/src/SigningUrl.php)
    - [object/vendor/highestate/bidding-messaging/src/SigningUrlReceive.php](object/vendor/highestate/bidding-messaging/src/SigningUrlReceive.php)



