### Highesatate installation

##### Install Components:
1. [Highesatate-3.0](README-highestate-3.0.md) git@bitbucket.org:highestate/highestate-3.0.git
2. [Mypage](README-mypage.md) git@bitbucket.org:highestate/mypage.git
3. [Object](README-object.md) git@bitbucket.org:highestate/object.git
4. [Highestateapi](README-highestateapi.md) git@bitbucket.org:highestate/object.git

##### Laradock installation
```bash
git clone --single-branch --branch local https://maksymch-tallium@bitbucket.org/maksymch-tallium/hemverket-laradock.git
```
Now your project structure should looks like below:

├── highestate-3.0\
├── mypage\
├── object\
├── highestateapi\
└── laradock

##### Run docker-compose
```bash
cd laradock
docker-compose up -d nginx mariadb
```

> After install go to the workspace:
```bash
docker-compose exec -u laradock workspace bash
```
> In your workspace install composer dependencies
```bash
cd highestate-3.0
composer install
cd ../

cd mypage
composer install
cd ../

cd object
composer install
cd ../
    
cd highestateapi
composer install

exit
```

> Rename files `laradock/nginx/*.conf.example` to `laradock/nginx/*.conf` \
> Rename `laradock/env-hemverket` to `laradock/.env` \
> Then run:
```bash
docker container exec hemverket_nginx_1 nginx -t
docker container exec hemverket_nginx_1 nginx -s reload
```



> Add to the end of `/etc/hosts` file next rows:
```bash
...
127.0.0.1 localhost.app.highestate.se
127.0.0.1 localhost.object.highestate.se
127.0.0.1 localhost.mypage.highestate.se
```

Ready to work.
